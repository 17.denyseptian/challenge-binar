-- MEMBUAT DATABASE --
CREATE DATABASE game;



--MEMBUAT TABEL USER--
--MEMBUAT DATA TYPE ENUM UNTUK ROLE--
CREATE TYPE role AS ENUM('Warrior', 'Mage', 'Archer');
CREATE TABLE users (
    username VARCHAR(12) PRIMARY KEY NOT NULL,
    password VARCHAR(16) NOT NULL,
    uid BIGSERIAL,
    role role
);

--MEMBUAT TABEL STATS--
CREATE TABLE stats (
    id BIGSERIAL PRIMARY KEY NOT NULL,
    uid INT,
    level INT NOT NULL,
    hp INT,
    def INT,
    strength INT,
    intelligence INT,
    agility INT,
    experience INT
);

--MEMBUAT TABEL ITEMS--
--MEMBUAT DATA TYPE UNTUK ENUM UNTUK ITEM TYPE--
CREATE TYPE item_type AS ENUM('Weapon', 'Armor', 'Others');
CREATE TABLE items(
    id BIGSERIAL PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL,
    level INT NOT NULL,
    type item_type NOT NULL,
    description TEXT,
    hp INT,
    def INT,
    strength INT,
    intelligence INT,
    agility INT
);

-- MEMBUAT TABEL EQUIPPED 
CREATE TABLE equipped(
    id BIGSERIAL PRIMARY KEY NOT NULL,
    uid INT,
    weapon INT,
    armor INT
);



--#INSERT DATA KEDALAM TABEL#--

--USERS--
INSERT INTO users (
    username, password, role
    ) 
VALUES 
    ('clayriel', 'inipassword', 'Warrior'),
    ('len', 'inijugapassword', 'Mage'),
    ('envy', '********', 'Archer'),
    ('yuza','a2cj2n58a242','Warrior');

--ITEMS--
INSERT INTO items(
    name, level, type, description, hp, def, strength, intelligence, agility
) VALUES 
('Baju Penyihir' , 5, 'Armor', 'Baju ini bisa buat kamu menghilang dari dunia', 400, 25, 0, 30, 5),
('Zirah Kuno', 5, 'Armor', 'Zirah ini sudah rapuh seperti hatimu', 500, 40, 30, 0, 5),
('Baju Robin Hood' , 5, 'Armor', 'Baju yang bikin kamu bisa lari cepat', 450, 30, 10 , 10, 30),
('Baju Biasa' , 1, 'Armor', 'Baju biasa biar kamu gk kedinginan', 100, 1, 0, 0, 0),
('Pedang Kayu', 1,'Weapon' , 'Pedang buat latihan', 0, 0, 10, 0, 0 ),
('Busur Bambu', 2, 'Weapon', 'Busur panah buat latihan', 0,0,15, 0,5),
('Keris Samsudin', 3, 'Weapon', 'Keris listrik buat boongin netijen', 0,0,0, 15,0),
('Serbuk Ajaib', 1, 'Others', 'Unknown', 0,0,0,0,0);

--USER STATS--
INSERT INTO stats(
    uid, level, hp, def, strength,intelligence,agility,experience
    )VALUES
    (1, 20, 1500, 0, 50, 0 , 10, 0),
    (2, 6, 1200, 0, 10, 50 , 5, 0),
    (3, 12, 1350, 0, 10, 10 , 40, 0),
    (4, 1, 1500, 0, 50, 0 , 10, 0);

    --EQUIPPED--
INSERT INTO equipped ( 
    uid, armor, weapon
    ) 
VALUES --memberikan baju biasa dan senjata kepada masing-masing user
    (1,4,5),
    (2,4,7),
    (3,4,6),
    (4,4,5);
    
--Membuat Stored Procedure untuk Equip item--
CREATE OR REPLACE PROCEDURE equip_weapon(
    id_user INT,
    item_id INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    --EQUIPPED ITEMS--
    UPDATE equipped 
    SET weapon = item_id
    WHERE uid = id_user;
    CALL update_stats(id_user, item_id);
    COMMIT;
END; $$;

CREATE OR REPLACE PROCEDURE equip_armor(
    id_user INT,
    item_id INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    --EQUIPPED ITEMS--
    UPDATE equipped 
    SET armor = item_id
    WHERE uid = id_user;
    CALL update_stats(id_user, item_id);
    COMMIT;
END; $$;


--UPDATE STATS PROCEDURE--
CREATE OR REPLACE PROCEDURE update_stats(id_user INT, item_id INT)
LANGUAGE plpgsql
AS $$
DECLARE 
    health_point INT ;
    defense INT;
    strngth INT;
    intl INT;
    agi INT;
BEGIN
    SELECT hp INTO health_point FROM items WHERE id = item_id;
    SELECT def INTO defense FROM items WHERE id = item_id;
    SELECT strength INTO strngth FROM items WHERE id = item_id;
    SELECT intelligence INTO intl FROM items WHERE id = item_id;
    SELECT agility INTO agi FROM items WHERE id = item_id;

 UPDATE stats SET 
    hp = hp + health_point,
    def = def + defense,
    strength = strength + strngth,
    intelligence = intelligence + intl,
    agility = agility + agi
    WHERE uid = id_user;
    COMMIT; 
END; $$;

--SELECT DATA--
SELECT 
    stats.uid AS ID, users.username AS Name,stats.level AS Lv, users.role AS Role,
    stats.hp AS HP, stats.def AS DEF, equipped.weapon AS WEAPON,
    equipped.armor AS ARMOR, stats.strength AS STR, stats.intelligence AS INT, stats.agility AS AGI
FROM stats JOIN equipped ON equipped.uid = stats.uid
LEFT JOIN users ON users.uid = stats.uid ORDER BY Lv DESC;


--SELL ITEM PROCEDURE--
CREATE OR REPLACE PROCEDURE sell_item(item_id INT)
LANGUAGE plpgsql
AS $$
BEGIN
    DELETE FROM items WHERE id = item_id;
    COMMIT; 
END; $$;