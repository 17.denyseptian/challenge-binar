const st = require("supertest");
const app = require("../app");

const userTest = {
  email: "tuyul3@test.com",
  username: "tuyul3",
  password: "root",
  user_id: 3,
  bio: "Bio after created",
  score: 17000,
};

const bioUpdateTest = {
  bio: "Bio After Updated",
};

var data = "";
var token = "";

const truncate = require("../helpers/truncate");
truncate.user();
truncate.userBio();
truncate.userHistory();

describe("/test/auth endpoint", () => {
  //test register berhasil
  test("Register Berhasil", async () => {
    try {
      const res = await st(app).post("/test/auth").send({
        email: userTest.email,
        username: userTest.username,
        password: userTest.password,
      });

      expect(res.statusCode).toBe(201);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe(true);
      expect(res.body.message).toBe("User Created!");
      expect(res.body.data).toStrictEqual({
        email: userTest.email,
        username: userTest.username,
      });
    } catch (error) {
      expect(error).toBe("error");
    }
  });

  // test gagal
  test("Register Gagal", async () => {
    try {
      const res = await st(app).post("/test/auth").send(userTest);

      expect(res.statusCode).toBe(400);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe(false);
      expect(res.body.message).toBe("Email Already Used");
      expect(res.body.data).toBe(null);
    } catch (error) {
      expect(error).toBe("error");
    }
  });
});

//test login
describe("/test/auth/login", () => {
  test("Login gagal", async () => {
    try {
      const res = await st(app)
        .post("/test/auth/login")
        .send({
          email: userTest.email,
          password: `${userTest.password}a`,
        });

      expect(res.statusCode).toBe(400);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe(false);
      expect(res.body.message).toBe("Email or Password doesn't match");
      expect(res.body.data).toBe(null);
    } catch (error) {
      expect(error).toBe("error");
    }
  });
  test("Login berhasil", async () => {
    try {
      const res = await st(app).post("/test/auth/login").send({
        email: userTest.email,
        password: userTest.password,
      });

      token = res.body.data.token;
      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.data).toHaveProperty("token");
      expect(res.body.status).toBe(true);
      expect(res.body.message).toBe("Success Login");
    } catch (error) {
      expect(error).toBe("error");
    }
  });
});

//test User Bio

//Get all user bio
describe("/test/userBio/ endpoint", () => {
  test("Success get all user bio", async () => {
    try {
      const res = await st(app)
        .get("/test/userBio")
        .then((res) => {
          data = res.body.data;
          expect(res.statusCode).toBe(200);
          expect(res.body).toHaveProperty("status");
          expect(res.body).toHaveProperty("message");
          expect(res.body).toHaveProperty("data");
          expect(res.body.status).toBe(true);
          expect(res.body.message).toBe("Success Get All Data");
          expect(res.body.data).toEqual(data);
        });
    } catch (error) {
      expect(error).toBe("error");
    }
  });
});

//Create user bio
describe("/test/userBio/add endpoint", () => {
  //create user bio berhasil
  test("Success Creating User Bio", async () => {
    try {
      const res = await st(app)
        .post("/test/userBio/add")
        .set({ Authorization: token })
        .send({
          bio: userTest.bio,
        });
      expect(res.statusCode).toBe(201);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe(true);
      expect(res.body.message).toBe("Bio Created");
      expect(res.body.data).toHaveProperty("username");
      expect(res.body.data).toHaveProperty("bio");
      expect(res.body.data).toHaveProperty("user_id");
      expect(res.body.data.username).toStrictEqual({
        username: userTest.username,
        bio: userTest.bio,
        user_id: userTest.user_id,
      });
    } catch (err) {
      expect(err).toBe("error");
    }
  });

  //create user bio gagal
  test("Failed creating user Bio", async () => {
    try {
      const res = await st(app)
        .post("/test/userBio/add")
        .set({ Authorization: token })
        .send({
          bio: userTest.bio,
        });

      expect(res.statusCode).toBe(400);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe(false);
      expect(res.body.message).toBe("Bio Already Created");
      expect(res.body.data).toBe(null);
    } catch (err) {
      expect(err).toBe("error");
    }
  });
});
