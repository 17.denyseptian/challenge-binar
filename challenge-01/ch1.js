//membuat kalkulator 

const { resolve } = require('path');
const rl = require ('readline');

const reqInput = rl.createInterface({
    input : process.stdin,
    output : process.stdout
});
    // input function
function input(question){
    return new Promise(resolve => {
        reqInput.question(question, (Val) => {
            resolve(Val);
        });
    });
}

//function
function sum (x , y ){
    return x+y;
     }
     function reduction (x , y){
       return x - y;
     }
   const division = function div (x , y){
       return x / y;
   }  
   const multiplication = function mult (x , y){
       return x * y;
   }  
   const exponential = (x) => x**2;  
   const rootSquare = (x) => Math.sqrt(x);
   const areaOfCircle =  (r) => 3.14 *r*r; 
   const areaOfParalelogram = (a , t) => a * t;   
   const cylinderVol = (r , t) => 3.14 *r*r *t;
   const mean = (total, valueLen) => total / valueLen;
      
   let value = [40,54, 25, 46, 37, 55, 57, 66, 67, 70, 80];
   let total = 0;
   let valueLen = value.length;
   for(let i = 0; i < value.length; i++){
       total +=value[i]
   }

async function calculator (){
    let x = await input('please insert x value : ');
    let y = await input('please insert y value : '); 
    console.log(`hasil bagi dari ${x}(x) dan ${y}(y) adalah` ,division(x, y));
    console.log(`hasil pangkat dari ${x}(x) adalah` , exponential(x));
    console.log(`hasil akar dari ${x}(x) adalah` , rootSquare(x));
    console.log(`luas lingkaran dengan jari-jari ${x}(x) adalah` , areaOfCircle(x));
    console.log(`luas jajargenjang dengan alas ${x}(x) dan tinggi ${y}(y) adalah` , areaOfParalelogram(x , y));
    console.log(`volume tabung dengan jari-jari ${x}(x) dan tinggi ${y}(y)  adalah` , cylinderVol(x, y));
    console.log(`Rata-rata dari ${value}   adalah `, mean(total , valueLen));
    reqInput.close();
}

calculator();