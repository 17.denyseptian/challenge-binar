const { UserBio } = require("../models");

module.exports = {
  create: async (req, res) => {
    try {
      const user = req.user;
      const { bio } = req.body;

      const exist = await UserBio.findOne({
        where: { username: user.username },
      });
      if (exist) {
        return res.status(400).json({
          status: false,
          message: "Bio Already Created",
          data: null,
        });
      }
      const create = await UserBio.create({
        username: user.username,
        bio,
        user_id: user.id,
      });
      return res.status(201).json({
        status: true,
        message: "Bio Created!",
        data: create,
      });
    } catch (error) {
      console.log(error.message);
    }
  },
  update: async (req, res) => {
    try {
      const user = req.user;
      const exist = await UserBio.findOne({
        where: { username: user.username },
      });

      if (!exist) {
        return res.status(400).json({
          status: false,
          message: "Bio Haven't Created",
          data: null,
        });
      }

      const updateBio = await UserBio.update(
        {
          bio: req.body.bio,
        },
        {
          where: {
            username: user.username,
          },
        }
      );

      const updated = await UserBio.findOne({
        where: { username: user.username },
      });
      return res.status(200).json({
        status: true,
        message: "Bio Updated",
        data: {
          username: updated.username,
          bio: updated.bio,
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  },
  getAll: async (req, res) => {
    try {
      const findAll = await UserBio.findAll();
      return res.status(200).json({
        status: true,
        message: "Success Get All Data",
        data: findAll,
      });
    } catch (error) {
      console.log(error.message);
    }
  },
  getDetail: async (req, res) => {
    try {
      const { id } = req.params;

      const found = await UserBio.findOne({ where: { id: id } });
      if (!found) {
        return res.status(400).json({
          status: false,
          message: "User not Found",
          data: null,
        });
      }

      return res.status(200).json({
        status: true,
        message: "Success Get Data",
        data: found,
      });
    } catch (error) {
      console.log(error.message);
    }
  },
  delete: async (req, res) => {
    try {
      const user = req.user;

      const deleteBio = await UserBio.destroy({
        where: { user_id: user.id },
      });

      return res.status(200).json({
        status: true,
        message: "Data Deleted!",
        data: deleteBio,
      });
    } catch (error) {
      console.log(error.message);
    }
  },
};
