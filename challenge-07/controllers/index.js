const auth = require("./auth");
const userBio = require("./userBio");
// const UserHistory = require("./userHistory");

module.exports = {
  auth,
  userBio,
};
