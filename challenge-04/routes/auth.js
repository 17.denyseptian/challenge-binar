const express = require("express");
const router = express.Router();
const c = require("../controllers");
const mid = require("../helper/middleware");

router.post("/register", c.auth.register);
router.post("/login", c.auth.login);
router.put("/update-password", mid.mustLogin, c.auth.updatePassword);
module.exports = router;
