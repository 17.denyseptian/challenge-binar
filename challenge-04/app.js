const express = require("express");
const app = express();
const router = require("./routes");
const morgan = require("morgan");
const { HTTP_PORT = 3000 } = process.env;

app.use(express.json());
app.use(morgan("dev"));
app.use(router);

//404 handler
app.use((req, res) => {
  return res.status(404).json({
    status: false,
    message: "Are you lost?",
  });
});

//500 handler
app.use(() => {
  return res.status(500).json({
    status: "",
  });
});

app.listen(HTTP_PORT, () => {
  console.log("Listening on port", HTTP_PORT);
});
