const adapter = require("../external/api-adapter");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const controller = require("../controllers");

const { USER_SERVICE_HOST } = process.env;

// use the self service instead of the class service host
const api = adapter(USER_SERVICE_HOST);

const user = {
  // meminta sebuah try catch block yang dimana dapat digunakan hal lainnya seperti .then.catch
  create: async (req, res, next) => {
    try {
      // check email
      const { nama, email, password, pekerjaan } = req.body;
      const { data } = await api.post("/auth/register", {
        nama,
        email,
        password,
        pekerjaan,
      });
      const user = data.data;
      return res.status(201).json({
        status: true,
        message: "User created - API Gateway",
        data: user,
      });
    } catch (error) {
      console.log(error.message);
    }
  },
};
module.exports = user;
