const classUser = require("./classUser");
const mentor = require("./mentor");
const user = require("./user");

module.exports = {
  classUser,
  mentor,
  user,
};
