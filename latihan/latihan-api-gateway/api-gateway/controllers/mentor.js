const adapter = require("../external/api-adapter");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const controller = require("../controllers");

const { CLASS_SERVICE_HOST } = process.env;

// use the self service instead of the class service host
const api = adapter(CLASS_SERVICE_HOST);

const mentor = {
  // meminta sebuah try catch block yang dimana dapat digunakan hal lainnya seperti .then.catch
  create: async (req, res, next) => {
    try {
      // check email
      const { nama, pekerjaan } = req.body;
      const { data } = await api.post("/mentor", { nama, pekerjaan }); // destruct the data for getting the status and data from the url

      const createdMentorData = data.data;

      return res.status(201).json({
        status: true,
        message: "success",
        data: createdMentorData,
      });
    } catch (err) {
      // check the connection
      if (err.code == "ECONNREFUSED") {
        err = new Error("service unavaiable"); // buat error terlebih dahulu lalu dilempar dengan next(err)
        return next(err);
      }

      if (err.response) {
        const { status, data } = err.response;
        res.status(status).json(data);
      }

      next(err);
    }
  },

  getMentorDetails: async (req, res, next) => {
    try {
      // check id
      const { id, nama } = req.body;
      // console.log(id);

      console.log(req.body);

      // destruct the data for getting the status and data from the url
      const { data } = await api.get("/mentor/details", { id, nama });

      // console.log(data);

      const mentor = data.data;

      return res.status(200).json({
        status: true,
        message: "Mentor Details",
        data: mentor,
      });
    } catch (err) {
      // console.log(err)
      // check the connection
      if (err.code == "ECONNREFUSED") {
        err = new Error("service unavaiable"); // buat error terlebih dahulu lalu dilempar dengan next(err)
        return next(err);
        // throw new Error('service unavailable');
      }
      // handle error dari service lain
      if (err.response) {
        const { status, data } = err.response;
        return res.status(status).json(data);
      }
      next(err);
    }
  },

  mentorAll: async (req, res, next) => {
    try {
      // destruct the data for getting the status and data from the url
      const { data } = await api.get("/mentor/all");

      // console.log(data);

      const allMentor = data.data;

      return res.status(200).json({
        status: true,
        message: "Mentor Details",
        data: allMentor,
      });
    } catch (err) {
      // console.log(err)
      // check the connection
      if (err.code == "ECONNREFUSED") {
        err = new Error("service unavaiable"); // buat error terlebih dahulu lalu dilempar dengan next(err)
        return next(err);
        // throw new Error('service unavailable');
      }
      // handle error dari service lain
      if (err.response) {
        const { status, data } = err.response;
        return res.status(status).json(data);
      }
      next(err);
    }
  },

  // Get All Mentor

  // Get the user name and password from the middleware
  // userDetails: (req, res, next) => {
  //     const user = req.user;

  //     try {
  //         return res.status(200).json({
  //             status: false,
  //             message: 'success',
  //             data: {
  //                 name: user.name,
  //                 email: user.email
  //             }
  //         });
  //     } catch (err) {
  //         next(err);
  //     }
  // },

  // cara untuk handling error
  // test: async (req, res, next) => {
  //     try {
  // Cara 1
  // try {
  //     const { data }  = await api.get('/test'); // destruct the data for getting the status and data from the url
  //     return res.send("Lakukan proses A")
  // } catch(err) {
  //     // check the connection
  //     if (err.code == 'ECONNREFUSED') {
  //         err = new Error('service unavaiable'); // buat error terlebih dahulu lalu dilempar dengan next(err)
  //         return next(err)
  //     }
  //     return res.send("Lakukan Proses B")
  // }

  // Cara 2
  // api.get('/test')
  //     .then(data => res.send("Lakukan Proses A"))
  //     .catch(err => {
  //         err
  //     });
  // } catch (err) {
  // next(err);
  // }
  // },
};
module.exports = mentor;
