const adapter = require("../external/api-adapter");

const { CLASS_SERVICE_HOST } = process.env;

// use the self service instead of the class service host
const api = adapter(CLASS_SERVICE_HOST);

const kelas = {
  // meminta sebuah try catch block yang dimana dapat digunakan hal lainnya seperti .then.catch
  create: async (req, res, next) => {
    try {
      // check email
      const { nama, deskripsi, mentor_id, level } = req.body;
      const { data } = await api.post("/classroom", {
        nama,
        deskripsi,
        mentor_id,
        level,
      }); // destruct the data for getting the status and data from the url

      const createdMentorData = data.data;

      return res.status(201).json({
        status: true,
        message: "success",
        data: createdMentorData,
      });
    } catch (err) {
      // check the connection
      if (err.code == "ECONNREFUSED") {
        err = new Error("service unavaiable"); // buat error terlebih dahulu lalu dilempar dengan next(err)
        return next(err);
      }

      if (err.response) {
        const { status, data } = err.response;
        res.status(status).json(data);
      }

      next(err);
    }
  },

  getClassDetails: async (req, res, next) => {
    try {
      // check id
      const { id } = req.body;
      console.log(id);

      // console.log(req);

      // destruct the data for getting the status and data from the url
      const { data } = await api.get("/classroom/details", { id });

      // console.log(data);

      const classroom = data.data;

      return res.status(200).json({
        status: true,
        message: "Classroom Details",
        data: classroom,
      });
    } catch (err) {
      // console.log(err)
      // check the connection
      if (err.code == "ECONNREFUSED") {
        err = new Error("service unavaiable"); // buat error terlebih dahulu lalu dilempar dengan next(err)
        return next(err);
        // throw new Error('service unavailable');
      }
      // handle error dari service lain
      if (err.response) {
        const { status, data } = err.response;
        return res.status(status).json(data);
      }
      next(err);
    }
  },

  classAll: async (req, res, next) => {
    try {
      // destruct the data for getting the status and data from the url
      const { data } = await api.get("/classroom/all");

      // console.log(data);

      const allClass = data.data;

      return res.status(200).json({
        status: true,
        message: "Mentor Details",
        data: allClass,
      });
    } catch (err) {
      // console.log(err)
      // check the connection
      if (err.code == "ECONNREFUSED") {
        err = new Error("service unavaiable"); // buat error terlebih dahulu lalu dilempar dengan next(err)
        return next(err);
        // throw new Error('service unavailable');
      }
      // handle error dari service lain
      if (err.response) {
        const { status, data } = err.response;
        return res.status(status).json(data);
      }
      next(err);
    }
  },

  // Get All Mentor

  // Get the user name and password from the middleware
  // userDetails: (req, res, next) => {
  //     const user = req.user;

  //     try {
  //         return res.status(200).json({
  //             status: false,
  //             message: 'success',
  //             data: {
  //                 name: user.name,
  //                 email: user.email
  //             }
  //         });
  //     } catch (err) {
  //         next(err);
  //     }
  // },

  // cara untuk handling error
  // test: async (req, res, next) => {
  //     try {
  // Cara 1
  // try {
  //     const { data }  = await api.get('/test'); // destruct the data for getting the status and data from the url
  //     return res.send("Lakukan proses A")
  // } catch(err) {
  //     // check the connection
  //     if (err.code == 'ECONNREFUSED') {
  //         err = new Error('service unavaiable'); // buat error terlebih dahulu lalu dilempar dengan next(err)
  //         return next(err)
  //     }
  //     return res.send("Lakukan Proses B")
  // }

  // Cara 2
  // api.get('/test')
  //     .then(data => res.send("Lakukan Proses A"))
  //     .catch(err => {
  //         err
  //     });
  // } catch (err) {
  // next(err);
  // }
  // },
};
module.exports = kelas;
