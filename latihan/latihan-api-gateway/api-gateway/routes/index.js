const express = require("express");
const router = express.Router();
const c = require("../controllers");

// mentor
router.post("/mentor/register", c.mentor.create);
router.get("/mentor/all-mentor", c.mentor.mentorAll);
router.get("/mentor/details", c.mentor.getMentorDetails);

//user
router.post("/user/register", c.user.create);
module.exports = router;
