const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { User } = require("../models");
const { JWT_SIGNATURE_KEY } = process.env;

const auth = {
  register: async (req, res) => {
    try {
      const { nama, email, password, pekerjaan } = req.body;

      const emailExist = await User.findOne({ where: { email } });
      if (emailExist) {
        return res.status(400).json({
          status: false,
          message: "Email Already Used!",
          data: null,
        });
      }
      const encryptedPassword = await bcrypt.hash(password, 10);
      const create = await User.create({
        nama,
        email,
        password: encryptedPassword,
        pekerjaan,
      });
      return res.status(201).json({
        status: true,
        message: "User Created",
        data: {
          nama: create.nama,
          email: create.email,
          pekerjaan: create.pekerjaan,
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  },
  login: async (req, res) => {
    try {
      const { email, password } = req.body;

      const user = await User.findOne({ where: { email: email } });
      if (!user) {
        return res.status(400).json({
          status: false,
          message: "Email or Password doesn't match",
        });
      }

      const correct = await bcrypt.compare(password, user.password);
      if (!correct) {
        return res.status(400).json({
          status: false,
          message: "Email or Password doesn't match",
        });
      }

      // const payload = {
      //   id: user.id,
      //   email: user.email,
      //   username: user.username,
      // };

      // const token = jwt.sign(payload, JWT_SIGNATURE_KEY);

      return res.status(200).json({
        status: true,
        message: "Success Login",
        // data: { token },
      });
    } catch (error) {
      console.log(error.message);
    }
  },
};
module.exports = auth;
