const express = require("express");
const router = express.Router();
const c = require("../controllers");

router.post("/", c.mentor.create);
router.get("/all", c.mentor.index);
router.get("/details", c.mentor.show);

module.exports = router;
