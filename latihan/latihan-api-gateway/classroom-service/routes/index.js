const express = require("express");
const router = express.Router();
const mentor = require("./mentor");
const kelas = require("./class");
const classUser = require("./classUser");

router.use("/mentor", mentor);
router.use("/class", kelas);
router.use("./class-user", classUser);

module.exports = router;
