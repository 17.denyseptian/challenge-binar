const express = require("express");
const router = express.Router();
const c = require("../controllers");

router.post("/", c.kelas.create);
router.get("/all", c.kelas.index);
router.get("/details", c.kelas.show);

module.exports = router;
