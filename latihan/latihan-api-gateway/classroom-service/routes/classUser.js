const express = require("express");
const router = express.Router();
const c = require("../controllers");

router.get("/all", c.classUser.index);
router.get("/:id", c.classUser.show);
router.post("/", c.classUser.create);

module.exports = router;
