const { Class } = require("../models");

const Kelas = {
  create: async (req, res, next) => {
    try {
      const { nama, deskripsi, mentor_id, level } = req.body;
      const existingClass = await Class.findOne({ where: { nama } });
      if (existingClass) {
        return res.status(400).json({
          status: false,
          message: "Class already exists",
        });
      }
      const newClass = await Class.create({
        nama,
        deskripsi,
        mentor_id,
        level,
      });
      res.status(201).json({
        status: "success",
        message: "Class created successfully",
        data: newClass,
      });
    } catch (err) {
      next(err);
    }
  },

  show: async (req, res, next) => {
    try {
      const { id } = req.body;

      const foundClass = await Class.findOne({ where: { id: id } });
      if (!foundClass) {
        return res.status(404).json({
          message: "Class not found",
        });
      }
      res.status(200).json({
        status: "success",
        message: "Class found",
        data: foundClass,
      });
    } catch (err) {
      next(err);
    }
  },

  index: async (req, res, next) => {
    try {
      const classes = await Class.findAll();
      res.status(200).json({
        status: "success",
        message: "Classes found",
        data: classes,
      });
    } catch (err) {
      next(err);
    }
  },
};

module.exports = Kelas;
