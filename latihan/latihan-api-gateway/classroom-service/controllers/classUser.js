const { User } = require("../models");
const { Class } = require("../models");
const { ClassUser } = require("../models");

const kelas_user = {
  create: async (req, res, next) => {
    try {
      const { user_id, class_id } = req.body;

      const user = await User.findByPk(user_id);

      if (!user) {
        return res.status(400).json({
          status: false,
          message: `There's no User!`,
          data: null,
        });
      }

      console.log(`${user_id}, ${class_id}`);

      const classroom = await Class.findByPk(class_id);

      if (!classroom) {
        return res.status(400).json({
          status: false,
          message: `There's no Classroom!`,
          data: null,
        });
      }

      const subscribe = await ClassUser.create({
        user_id,
        class_id,
      });

      return res.status(201).json({
        status: true,
        message: "success",
        data: subscribe,
      });
    } catch (err) {
      next(err);
    }
  },

  show: async (req, res, next) => {
    try {
      const where = {};
      const { id, email } = req.body;
      if (id) {
        where.id = id;
      }
      if (email) {
        where.email = email;
      }

      const user = await User.findOne({ where });
      if (!user) {
        return req.status(400).json({
          status: false,
          message: "not found!",
          data: null,
        });
      }

      return res.status(200).json({
        status: true,
        message: "success",
        data: {
          id: user.id,
          name: user.name,
          email: user.email,
          password: user.password,
        },
      });
    } catch (err) {
      next(err);
    }
  },

  index: async (req, res, next) => {
    try {
    } catch (err) {}
  },
};

module.exports = kelas_user;
