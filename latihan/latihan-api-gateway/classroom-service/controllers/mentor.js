const { Mentor } = require("../models");
const mentor = {
  create: async (req, res, next) => {
    try {
      const { nama, pekerjaan } = req.body;

      const mentorExist = await Mentor.findOne({ where: { nama } });

      if (mentorExist) {
        return res.status(404).json({
          status: false,
          message: `Mentor already exist`,
          data: null,
        });
      }

      const createMentor = await Mentor.create({
        nama,
        pekerjaan,
      });

      return res.status(201).json({
        status: true,
        message: "New mentor successfully added",
        data: createMentor,
      });
    } catch (err) {
      next(err);
    }
  },

  show: async (req, res, next) => {
    try {
      const where = {};

      const { id, nama } = req.body;

      if (id) {
        where.id = id;
      }

      if (nama) {
        where.nama = nama;
      }

      const foundMentor = await Mentor.findOne({ where });

      if (!foundMentor) {
        return res.status(404).json({
          status: false,
          message: "Mentor not found",
        });
      }

      return res.status(200).json({
        status: true,
        message: "Mentor found",
        data: foundMentor,
      });
    } catch (err) {
      next(err);
    }
  },

  index: async (req, res, next) => {
    try {
      const mentors = await Mentor.findAll();
      return res.status(200).json({
        status: true,
        message: "Classes found",
        data: mentors,
      });
    } catch (err) {
      next(err);
    }
  },
};

module.exports = mentor;
