const kelas = require("./class");
const mentor = require("./mentor");
const classUser = require("./classUser");

module.exports = { kelas, mentor, classUser };
