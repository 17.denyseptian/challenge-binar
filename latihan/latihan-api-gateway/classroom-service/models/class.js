'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Class extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Class.init({
    nama: DataTypes.STRING,
    deskripsi: DataTypes.STRING,
    mentor_id: DataTypes.INTEGER,
    level: DataTypes.ENUM('beginner', 'intermediate', 'expert')
  }, {
    sequelize,
    modelName: 'Class',
  });
  return Class;
};