const request = require("supertest");
const app = require("../app");

//endponit get
describe("base.index function", () => {
  // case success
  test("res.json called with {status: true, message : hello world!}", (done) => {
    request(app)
      .get("/")
      .then((res) => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty("status");
        expect(res.body).toHaveProperty("message");
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe("hello world!");
        done();
      });
    //   .then((err) => {
    //     console.log(err);
    //   });
  });
});

//endpoint POST
describe("base.sum function", () => {
  // case success
  test("res.json return summary of x + y", async () => {
    try {
      const x = 15;
      const y = 25;
      const result = x + y;
      const res = await request(app).post("/sum").send({ x, y });

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe(true);
      expect(res.body.message).toBe("parameters summarized!");
      expect(res.body.data).toBe({ x, y, result });
      //   expect(res.body).toEqual({
      //     status: true,
      //     message: "parameters summarized!",
      //     data: {
      //       x: req.body.x,
      //       y: req.body.y,
      //       result: req.body.x + req.body.y,
      //     },
      //   });

      done();
    } catch (err) {
      console.log(err);
    }

    //   .then((err) => {
    //     console.log(err);
    //   });
  });
});
