const express = require("express");
const app = express();
const port = 3001;
const morgan = require("morgan");
const controllers = require("./controllers");
const router = require("./routes");

app.use(morgan("dev"));
app.use(express.json()); //
app.use("/public", express.static("public"));
app.set("view engine", "ejs");

//home
app.get("/", (req, res) => res.render("home"));
//error
app.get("/error", (req, res) => Error);

app.get("/", (_req, res) => {
  res.status(200).json({
    status: "SUCCESSSSSSS",
    message: "welcome to unkown API",
  });
});

app.use(router);
app.use(controllers.exception);
app.use(controllers.notFound);

app.listen(port, () => {
  console.log("Server started on port 3001");
});
