const data = require("../data.json");
const fs = require("fs");

module.exports = {
  getAll: (req, res) => {
    const users = data.users;

    return res.json({
      data: users,
    });
  },
  getDetail: (req, res) => {
    const { userId } = req.params;
    const user = data.users.find((e) => e.id == userId);

    if (user == undefined) {
      return res.status(404).send("Not Found");
    }
    return res.status(200).json({
      status: "success",
      message: "berikut adalah detail dari user yang anda cari",
      data: user,
    });
  },
  create: (req, res) => {
    const { name, email } = req.body;
    let users = data.users;
    const user = {
      id: users[users.length - 1].id + 1,
      name,
      email,
    };
    users.push(user);
    fs.writeFileSync("./data.json", JSON.stringify(data));

    return res.status(201).json({
      status: "success",
      data: user,
    });
  },
  update: (req, res) => {
    const { userId } = req.params;
    const { name, email } = req.body;

    const found = data.users.find((e) => e.id == userId);
    if (found == undefined) {
      return res.status(404).json({
        message: "not found",
      });
    }
    if (name != undefined) {
      found.name = name;
    } else {
      found.name = found.name;
    }
    if (email != undefined) {
      found.email = email;
    } else {
      found.email = found.email;
    }
    fs.writeFileSync("./data.json", JSON.stringify(data));

    return res.status(201).json({
      data: found,
    });
  },

  delete: (req, res) => {
    const { userId } = req.params;
    const findIndex = data.users.find((e) => e.id == userId);
    console.log(findIndex);
    let user = data.users;
    // console.log(user);
    if (findIndex == undefined) {
      return res.status(404).send("ID not found");
    }

    // // user.splice(user[findIndex], 1);
    // console.log(findIndex);
    user = user.filter((e) => e.id != findIndex.id);
    fs.writeFileSync("./data.json", JSON.stringify(data));

    return res.status(200).json({
      message: "data deleted",
      data: user,
    });
  },
};
