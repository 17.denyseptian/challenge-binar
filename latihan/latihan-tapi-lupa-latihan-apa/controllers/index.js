const user = require("./user");
const product = require("./product");

module.exports = {
  user,
  product,
  exception: (err, req, res, next) => {
    res.send("server-error");
  },
  notFound: (req, res, next) => {
    res.send("Page not Found");
  },
};
