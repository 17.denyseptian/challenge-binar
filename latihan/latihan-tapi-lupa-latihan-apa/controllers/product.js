const data = require("../data.json");
const fs = require("fs");
const { product } = require(".");

module.exports = {
  getAll: (req, res) => {
    const products = data.products;

    return res.json({
      data: products,
    });
  },
  getDetail: (req, res) => {
    const { productId } = req.params;
    const item = data.products.find((e) => e.id == productId);

    if (item == undefined) {
      return res.status(404).send("Not Found");
    }
    return res.status(200).json({
      status: "success",
      message: "berikut adalah detail dari produk yang anda cari",
      data: item,
    });
  },
  create: (req, res) => {
    const { name, description, price } = req.body;
    let items = data.products;
    const item = {
      id: items[items.length - 1].id + 1,
      name,
      description,
      price,
    };
    items.push(item);
    fs.writeFileSync("./data.json", JSON.stringify(data, null, 2));

    return res.status(201).json({
      status: "success",
      data: item,
    });
  },
  update: (req, res) => {
    const { productId } = req.params;
    const { name, description, price } = req.body;

    const found = data.products.find((e) => e.id == productId);
    if (found == undefined) {
      return res.status(404).json({
        message: "not found",
      });
    }
    if (name != undefined) {
      found.name = name;
    } else {
      found.name = found.name;
    }
    if (description != undefined) {
      found.description = description;
    } else {
      found.description = found.description;
    }
    if (price != undefined) {
      found.price = price;
    } else {
      found.price = found.price;
    }
    fs.writeFileSync("./data.json", JSON.stringify(data));

    return res.status(201).json({
      data: found,
    });
  },

  delete: (req, res) => {
    const { productId } = req.params;
    const findIndex = data.products.find((e) => e.id == productId);

    let item = data.products;
    // console.log(item);
    if (findIndex == undefined) {
      return res.status(404).send("ID not found");
    }
    // // item.splice(item[findIndex], 1);
    item = item.filter((e) => e.id != findIndex.id);
    fs.writeFileSync("./data.json", JSON.stringify(data));

    return res.status(200).json({
      message: "data deleted",
      data: item,
    });
  },
};
