const express = require("express");
const router = express.Router();
const user = require("./user");
const product = require("./product");
const { route } = require("./product");

router.use("/users", user);
router.use("/products", product);

module.exports = router;
