const express = require("express");
const controller = require("../controllers");
const router = express.Router();

router.get("/", controller.product.getAll);
router.get("/:productId", controller.product.getDetail);
router.post("/", controller.product.create);
router.put("/:productId", controller.product.update);
router.delete("/:productId", controller.product.delete);
module.exports = router;
