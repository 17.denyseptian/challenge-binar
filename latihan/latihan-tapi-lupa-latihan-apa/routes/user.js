const express = require("express");
const controller = require("../controllers");
const router = express.Router();

router.get("/", controller.user.getAll);
router.get("/:userId", controller.user.getDetail);
router.post("/", controller.user.create);
router.put("/:userId", controller.user.update);
router.delete("/:userId", controller.user.delete);
module.exports = router;
