// CHALLENGE - 02  

const { resolve } = require('path');
const rl = require ('readline');

const reqInput = rl.createInterface({
    input : process.stdin,
    output : process.stdout
});
    // input function
function input(question){
    return new Promise(resolve => {
        reqInput.question(question, (Val) => {
            resolve(Val);
        });
    });
}

let array = [];

function showResult(){
    console.clear();
    console.log('Nilai yang telah kamu simpan adalah : \n', array.join(", "))
            showMinMax();
            showAvg(array);
            showPass(array);
            // showHigh(array); //unsorted 
            showSorted(array);
            showHigh(array) // sorted
}   

function pushArray(y){
    if(y != "q" ){
        array.push(+y);
        console.clear();
        console.log(`Kamu menambahkan ${y} , Nilai saat ini adalah : \n `)
        console.log(array);
    }
}
function showMinMax(){
    console.log(`Nilai tertinggi adalah: ${Math.max(...array)}`)
    console.log(`Nilai terendah adalah: ${Math.min(...array)}`)
}
function showAvg(arr){
    let total = 0;
    for(let i = 0; i < arr.length; i++) {
         total += arr[i];
        }
    let avg = total / arr.length;
    console.log(`Nilai rata-ratanya adalah : ${avg}`)
}
function showPass(arr){
    let pass = 0;
    let notPass = 0;
    for(let i = 0; i < arr.length; i++) {
        if(arr[i] >= 60){
            pass += 1;
        }else{
            notPass +=1;
        }
       }
       console.log(`Jumlah Siswa Lulus : ${pass}, Tidak Lulus: ${notPass}`)
}
function showHigh(arr){
    let high = [];
    for(let i = 0; i < arr.length; i++) {
         if(arr[i] == 90 || arr[i] == 100){
            high.push(arr[i]);
         }
        }
    console.log(`Nilai tinggi :  ${high.join()}`)
}
function showSorted(arr){
    let sortedArr = arr.sort(function(a,b){return(a-b)})
    // let min = sortedArr[0]
    // let max = sortedArr[arr.length-1]
    console.log(`Urutan nilai dari terendah ke tertinggi : ${sortedArr.join(", ")}` )
    // console.log(`Nilai terendahnya adalah ${min} dan tertingginya adalah ${max}`)
}
async function inArr(){
    let inn = await input("Masukkan Nilai : ");
    while (inn != "q" && Number.isInteger(+inn)== true){
        pushArray(+inn);
        

        inn = await input("Masukkan Nilai : ");
        if(inn != "q" && Number.isInteger(+inn)== true){
            inArr();
        }else if(inn == "q"){
            console.clear()
            console.log("Input selesai, berikut adalah nilai yang telah dimasukkan: \n" ,  array);
        }else {
            console.clear()
            console.log(`Nilai yang dimasukkan harus berupa angka, kamu memasukkan "${inn}"`);
            inArr();
        }
    }
    if(inn == "q"){
        console.clear()
        console.log("Input selesai, berikut adalah nilai yang telah dimasukkan: \n" ,  array);
        showResult();
    }else{
        console.clear()
        console.log(`Nilai yang dimasukkan harus berupa angka, kamu memasukkan "${inn}"`);
    inArr();
    }
    }

inArr();
