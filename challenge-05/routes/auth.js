const express = require("express");
const router = express.Router();
const c = require("../controllers");

router.post("/", c.auth.create);
router.post("/login", c.auth.login);

module.exports = router;
