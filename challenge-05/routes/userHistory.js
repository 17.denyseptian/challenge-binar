const express = require("express");
const router = express.Router();
const c = require("../controllers");
const mid = require("../helper/middleware");

router.post("/", mid.mustLogin, c.UserHistory.create);
router.get("/all", c.UserHistory.getAll);
router.get("/:id", c.UserHistory.getDetail);
module.exports = router;
