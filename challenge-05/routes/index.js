const express = require("express");
const router = express.Router();
const auth = require("./auth");
const userBio = require("./userBio");
const userHistory = require("./userHistory");

router.use("/auth", auth);
router.use("/userBio", userBio);
router.use("/history", userHistory);

module.exports = router;
