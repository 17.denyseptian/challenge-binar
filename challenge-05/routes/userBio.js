const express = require("express");
const router = express.Router();
const c = require("../controllers");
const mid = require("../helper/middleware");

router.post("/", mid.mustLogin, c.userBio.create);
router.put("/", mid.mustLogin, c.userBio.update);
router.get("/all", mid.mustLogin, c.userBio.getAll);
router.get("/:id", mid.mustLogin, c.userBio.getDetail);
router.delete("/", mid.mustLogin, c.userBio.delete);

module.exports = router;
